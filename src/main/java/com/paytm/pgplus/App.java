package com.paytm.pgplus;

import com.paytm.pgplus.model.MerchantData;
import com.paytm.pgplus.service.MerchantDataService;
import com.paytm.pgplus.service.ProdMidFetcher;
import com.paytm.pgplus.utils.JiraUtil;
import com.paytm.pgplus.utils.JobIdGenerator;

import java.util.List;

public class App {
    public static void main(String[] args) {
        String jobId = JobIdGenerator.getUniqueJobId();
        System.out.println("Processing started for job Id : "+jobId);
        List<String> merchantIds = ProdMidFetcher.getProdMidList(jobId);
        List<MerchantData> merchantDataList = MerchantDataService.getMerchantData(jobId,merchantIds);
        if(merchantDataList.isEmpty()) {
            return;
        }
        JiraUtil jiraUtil = new JiraUtil();
        if (jiraUtil.doCreateJiraTicket()) {
            jiraUtil.createJiraTicket(jobId,merchantDataList);
        } else {
            jiraUtil.getJiraDescription(jobId, merchantDataList);
            System.out.println("Job completed without creating Jira ticket. Please check the report for insert sql query.");
        }
    }
}
