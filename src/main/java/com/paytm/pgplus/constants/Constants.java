package com.paytm.pgplus.constants;
public class Constants {
    public static String PROD_MID_LIST_FILE_PATH = "prod.mid.list.file.path";
    public static String MAPPING_SERVICE_URL = "mapping.service.url";
    public static String MAPPING_SERVICE_MERCHANT_PROFILE_URL = "mapping.service.merchant.profile.url";
    public static String MAPPING_SERVICE_MERCHANT_DATA_URL = "mapping.service.merchant.data.url";

    public static String JIRA_CREATE_TICKET="jira.create.ticket";
    public static String JIRA_HOST = "jira.host";
    public static String JIRA_USER = "jira.user";
    public static String JIRA_PASSWORD = "jira.password";

    public static String JIRA_CREATE_TICKET_URL = "jira.create.ticket.url";

    public static String JIRA_PGDBA_ASSIGNEE_USER_NAME = "jira.pgpdba.assignee.user.name";
    public static String JIRA_BUSINESS_REQUIREMENT = "jira.business.requirement";
    public static String JIRA_CC_USERIDS = "jira.cc.userid";
    public static String JIRA_TICKET_PRIORITY = "jira.ticket.priority";
    public static String JIRA_TICKET_SUMMARY = "jira.ticket.summary";

    public static String JIRA_PGDBA_PROJECT_ID = "20746"; // PGDBA
    public static String JIRA_ISSUE_TYPE_TASK_ID = "3"; //Task

    /*public static String JIRA_PGDBA_PROJECT_ID = "10000"; //
    public static String JIRA_ISSUE_TYPE_TASK_ID = "10001"; //Task*/
}
