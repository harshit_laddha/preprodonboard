package com.paytm.pgplus.model;

import com.paytm.pgplus.constants.Constants;
import com.paytm.pgplus.utils.PropertiesUtil;

public class JiraTicket {
    private String id;
    private String key;
    private String self;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getSelf() {
        return self;
    }

    public String getJiraTicketUrl() {
        String host = PropertiesUtil.getProperty(Constants.JIRA_HOST);
        if (host.endsWith("/")) {
            host = host.substring(0, host.length() - 1);
        }
        return host + "/browse/"+this.getKey();
    }
}
