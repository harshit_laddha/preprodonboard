package com.paytm.pgplus.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantData implements Serializable {
    private Long id;
    private String paytmId;
    private String paytmWalletId;
    private String alipayId;
    private String alipayWalletId;
    private String contractPayload;
    private String officialName;
    private Long industryTypeId;
    private String businessName;
    private String merchantType;

    public MerchantData() {
    }

    public Long getId() {
        return id;
    }

    public String getPaytmId() {
        return paytmId;
    }

    public String getPaytmWalletId() {
        return paytmWalletId;
    }

    public String getAlipayId() {
        return alipayId;
    }

    public String getAlipayWalletId() {
        return alipayWalletId;
    }

    public String getContractPayload() {
        return contractPayload;
    }

    public String getOfficialName() {
        return officialName;
    }

    public Long getIndustryTypeId() {
        return industryTypeId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public String getMerchantType() { return merchantType; }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }
}
