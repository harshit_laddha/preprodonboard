package com.paytm.pgplus.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MerchantProfile implements Serializable {
    private String merchantType;

    public MerchantProfile() {
    }

    public String getMerchantType() {
        return merchantType;
    }
}
