package com.paytm.pgplus.model;

public enum MerchantType {
    INDIVIDUAL,
    CORPORATION,
    FINANCIAL_INST,
    UNORGANISED,
    ORGANISED;

    private MerchantType() {
    }
}