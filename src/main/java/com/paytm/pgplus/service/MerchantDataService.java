package com.paytm.pgplus.service;

import com.paytm.pgplus.constants.Constants;
import com.paytm.pgplus.model.MerchantData;
import com.paytm.pgplus.model.MerchantProfile;
import com.paytm.pgplus.utils.PropertiesUtil;
import com.paytm.pgplus.utils.ReportGenerator;
import com.paytm.pgplus.utils.RequestClient;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class MerchantDataService {

    public static List<MerchantData> getMerchantData(String jobId,List<String> merchantIds) {
        List<MerchantData> merchantDataList = new ArrayList<>();
        List<String> errorMerchantIdList = new ArrayList<>();
        for(String merchantId : merchantIds) {
            MerchantData merchantData = null;
            try {
                merchantData = getMerchantData(merchantId);
                if(null != merchantData) {
                    getMerchantType(merchantData);
                    merchantDataList.add(merchantData);
                } else {
                    errorMerchantIdList.add(merchantId);
                }
            } catch (SocketTimeoutException e) {
                System.out.println("ERROR : Cannot connect to Mapping Service. Aborting.");
                System.out.println("Please check your internet connectivity and ensure that you are connected to VPN and then try again.");
                break;
            }
        }

        if (merchantDataList.isEmpty()) {
            System.out.println("No Valid MIDs to fetch. Terminating the process.");
        }

        if (!errorMerchantIdList.isEmpty()) {
            System.out.println("Failed to fetch "+ errorMerchantIdList.size() + " mids. Please check the report for details.");
            ReportGenerator reportGenerator = new ReportGenerator(jobId);
            reportGenerator.reportFailedMid(errorMerchantIdList);
        }
        return merchantDataList;
    }

    private static void getMerchantType(MerchantData merchantData) throws SocketTimeoutException {
        String mappingServiceBaseUrl = PropertiesUtil.getProperty(Constants.MAPPING_SERVICE_URL);
        String merchantProfileUrl = PropertiesUtil.getProperty(Constants.MAPPING_SERVICE_MERCHANT_PROFILE_URL).replace("{pgmid}",merchantData.getPaytmId());
        String urlString = mappingServiceBaseUrl + merchantProfileUrl;
        MerchantProfile merchantProfile = RequestClient.sendGetRequest(urlString,MerchantProfile.class);
        merchantData.setMerchantType(merchantProfile.getMerchantType());
    }

    private static MerchantData getMerchantData(String merchantId) throws SocketTimeoutException {
        String mappingServiceBaseUrl = PropertiesUtil.getProperty(Constants.MAPPING_SERVICE_URL);
        String merchantDataUrl = PropertiesUtil.getProperty(Constants.MAPPING_SERVICE_MERCHANT_DATA_URL);
        String urlString = mappingServiceBaseUrl + merchantDataUrl + merchantId;
        return RequestClient.sendGetRequest(urlString,MerchantData.class);
    }
}
