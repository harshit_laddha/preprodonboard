package com.paytm.pgplus.service;

import com.paytm.pgplus.constants.Constants;
import com.paytm.pgplus.utils.PropertiesUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ProdMidFetcher {

    public static List<String> getProdMidList(String jobId) {
        List<String> prodMidList = new ArrayList<>();
        String filePath = PropertiesUtil.getProperty(Constants.PROD_MID_LIST_FILE_PATH);
        File file = new File(filePath);
        try {
            Scanner sc = new Scanner(file);
            while(sc.hasNextLine()) {
                prodMidList.addAll(Arrays.asList(sc.nextLine().split(",")));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found : "+ filePath);
        }
        return prodMidList;
    }
}
