package com.paytm.pgplus.utils;

import com.paytm.pgplus.constants.Constants;
import com.paytm.pgplus.model.JiraTicket;
import com.paytm.pgplus.model.MerchantData;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.List;

public class JiraUtil {

    private final String host;
    private final String authHeader;

    public JiraUtil() {
        String host = PropertiesUtil.getProperty(Constants.JIRA_HOST);
        this.authHeader = getAuth();
        if (host.endsWith("/")) {
            this.host = host.substring(0, host.length() - 1);
        } else {
            this.host = host;
        }
    }

    public void createJiraTicket(String jobId, List<MerchantData> merchantDataList) {
        ReportGenerator reportGenerator = new ReportGenerator(jobId);
        System.out.println("Creating Jira Ticket for Job Id : "+jobId);
        String jiraDescription = getJiraDescription(jobId, merchantDataList);
        try {
            JSONObject jiraPayload = createJiraPayload(jiraDescription);
            JiraTicket jiraTicket = RequestClient.sendPostRequest(getCreateIssueUrl(), jiraPayload,getAuth(), JiraTicket.class);

            if (null == jiraTicket) {
                System.out.println("Failed to create Jira Ticket.");
                System.out.println("You can try creating the ticket manually. Jira description has been saved in the report file.");
                return;
            }
            String jiraTicketUrl = jiraTicket.getJiraTicketUrl();
            System.out.println("\nJira Created Successfully\n");
            System.out.println(jiraTicketUrl);
            reportGenerator.reportMessage("Jira Created Successfully \n",jiraTicketUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
        }
    }

    public String getJiraDescription(String jobId,List<MerchantData> merchantDataList) {
        StringBuilder builder = new StringBuilder();
        builder.append("Please execute this queries on pgp preprod db:\n\n");

        for(MerchantData merchantData : merchantDataList) {
            builder.append(SqlQueryGenerator.getInsertQueryForMerchant(merchantData));
        }
        String jiraDescription = builder.toString();
        ReportGenerator reportGenerator = new ReportGenerator(jobId);
        reportGenerator.reportMessage("Jira Description",jiraDescription);
        return jiraDescription;
    }

    private JSONObject createJiraPayload(String description) throws JSONException {
        JSONObject issue = new JSONObject();
        JSONObject fields = new JSONObject();
        fields.put("project", new JSONObject().put("id", Constants.JIRA_PGDBA_PROJECT_ID));
        fields.put("issuetype", new JSONObject().put("id", Constants.JIRA_ISSUE_TYPE_TASK_ID));
        fields.put("summary", PropertiesUtil.getProperty(Constants.JIRA_TICKET_SUMMARY));
        fields.put("description", description);
        fields.put("priority", new JSONObject().put("id", PropertiesUtil.getProperty(Constants.JIRA_TICKET_PRIORITY)));
        setAssignee(fields);
        setCustomFields(fields);
        issue.put("fields", fields);
        return issue;
    }

    private void setCustomFields(JSONObject fields) throws JSONException {
        setDatabase(fields);
        setEnvironment(fields);
        setBusinessRequirement(fields);
        setCCUser(fields);
    }

    private void setAssignee(JSONObject fields) throws JSONException {
        JSONObject assignee = new JSONObject();
        assignee.put("name",PropertiesUtil.getProperty(Constants.JIRA_PGDBA_ASSIGNEE_USER_NAME));
        fields.put("assignee",assignee);
    }

    private void setDatabase(JSONObject fields) throws JSONException {
        JSONObject database = new JSONObject();
        database.put("id","12509");
        database.put("value","preprodpgplus");

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(database);
        fields.put("customfield_13803",jsonArray);
    }

    private void setEnvironment(JSONObject fields) throws JSONException {
        JSONObject environment = new JSONObject();
        environment.put("id","12502");
        environment.put("value","PreProd");

        JSONArray jsonArray = new JSONArray();
        jsonArray.put(environment);
        fields.put("customfield_13802",jsonArray);
    }

    private void setBusinessRequirement(JSONObject fields) throws JSONException {
        fields.put("customfield_13570",PropertiesUtil.getProperty(Constants.JIRA_BUSINESS_REQUIREMENT));
    }

    private void setCCUser(JSONObject fields) throws JSONException {
        String[] users = PropertiesUtil.getProperty(Constants.JIRA_CC_USERIDS).split(",");
        JSONArray jsonArray = new JSONArray();
        for(String user : users) {
            JSONObject userObj = new JSONObject();
            userObj.put("name",user);
            jsonArray.put(userObj);
        }
        fields.put("customfield_10901",jsonArray);
    }

    private String getAuth() {
        String username = PropertiesUtil.getProperty(Constants.JIRA_USER);
        String password = PropertiesUtil.getProperty(Constants.JIRA_PASSWORD);
        try {
            String s = username + ":" + password;
            byte[] byteArray = s.getBytes();
            String auth;
            auth = Base64.encodeBase64String(byteArray);

            return auth;
        } catch (Exception ignore) {
            return "";
        }
    }

    private String getCreateIssueUrl() {
        return this.host + PropertiesUtil.getProperty(Constants.JIRA_CREATE_TICKET_URL);
    }

    public Boolean doCreateJiraTicket() {
        return "Yes".equalsIgnoreCase(PropertiesUtil.getProperty(Constants.JIRA_CREATE_TICKET));
    }
}
