package com.paytm.pgplus.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class JobIdGenerator {
    public static String getUniqueJobId() {
        Date date = new Date();
        //SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmssMs");
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyhhmmss");
        String jobId = sdf.format(date);
        return jobId;
    }
}
