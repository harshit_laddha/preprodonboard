package com.paytm.pgplus.utils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Property {
    private static Property obj;
    private static Properties properties;

    private Property(){
        try {
            FileReader reader = new FileReader("application.properties");
            properties = new Properties();
            properties.load(reader);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Property getInstance() {
        if (obj == null) {
            synchronized (Property.class) {
                if(obj == null) {
                    obj = new Property();
                }
            }
        }
        return obj;
    }

    public String getProperty(String propertyKey) {
        return properties.getProperty(propertyKey);
    }
}
