package com.paytm.pgplus.utils;

import com.paytm.pgplus.model.MerchantData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ReportGenerator {

    private String jobId;
    private String reportFileName;
    private String SECTION_SEPERATOR = "\n\n##############################################\n\n";

    public ReportGenerator(String jobId) {
        this.jobId = jobId;
        this.reportFileName = jobId + ".log";
        File reportFile = new File(this.reportFileName);
        try {
            reportFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reportFailedMid(List<String> merchantIds) {
        Path reportFile = Paths.get(reportFileName);
        try {
            Files.write(reportFile,("\nTotal Failed Mids : "+merchantIds.size()+"\n").getBytes(), StandardOpenOption.APPEND);
            if (!merchantIds.isEmpty()) {
                Files.write(reportFile,merchantIds, StandardCharsets.UTF_8,StandardOpenOption.APPEND);
            }
            Files.write(reportFile,SECTION_SEPERATOR.getBytes(),StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reportMessage(String title, String message) {
        Path reportFile = Paths.get(reportFileName);
        try {
            Files.write(reportFile,(title+"\n\n").getBytes(),StandardOpenOption.APPEND);
            Files.write(reportFile,message.getBytes(),StandardOpenOption.APPEND);
            Files.write(reportFile,SECTION_SEPERATOR.getBytes(),StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
