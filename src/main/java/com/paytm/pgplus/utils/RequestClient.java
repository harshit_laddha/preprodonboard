package com.paytm.pgplus.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;

public class RequestClient {

    public static <T> T sendGetRequest(String urlString, Class<T> responseClass) throws SocketTimeoutException {
        URL url = null;
        try {
            url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("accept", "application/json");
            connection.setConnectTimeout(5000);
            InputStream responseStream = connection.getInputStream();
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
            return mapper.readValue(responseStream, responseClass);
        } catch (SocketTimeoutException e) {
            throw new SocketTimeoutException("Cannot connect to "+urlString);
        } catch (Exception e) {
            return null;
        }
    }

    public static <T> T sendPostRequest(String urlString, JSONObject payload, String token, Class<T> responseClass) throws SocketTimeoutException {
        URL url = null;
        try {
            url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("accept", "application/json");
            connection.setRequestProperty("Accept-Encoding", "gzip");
            connection.setConnectTimeout(5000);
            if(token != null) {
                connection.setRequestProperty("Authorization", "Basic "+token);
            }
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            try (OutputStream os = connection.getOutputStream()) {
                byte[] input = payload.toString().getBytes(StandardCharsets.UTF_8);
                os.write(input,0, input.length);
            }

            try(BufferedReader br = new BufferedReader(new InputStreamReader(new GZIPInputStream(connection.getInputStream())))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                ReportGenerator rg = new ReportGenerator("1");
                rg.reportMessage("jira ticket response",response.toString());
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(response.toString().trim().getBytes(StandardCharsets.UTF_8),responseClass);
            }
        } catch (SocketTimeoutException e) {
            throw new SocketTimeoutException("Cannot connect to "+urlString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
