package com.paytm.pgplus.utils;

import com.paytm.pgplus.model.MerchantData;

public class SqlQueryGenerator {

    private static String SINGLE_QUOTE = "'";

    public static String getInsertQueryForMerchant(MerchantData merchantData) {
        StringBuilder builder = new StringBuilder();
        builder.append("insert into alipay_paytm_merchant (alipay_merchant_id,paytm_merchant_id,merchant_type," +
                "official_name,create_timestamp,update_timestamp,updated_count,industry_type_id)"+
                " VALUES(");
        builder.append(merchantData.getAlipayId()).append(",");
        builder.append(SINGLE_QUOTE).append(merchantData.getPaytmId()).append(SINGLE_QUOTE).append(",");
        builder.append(SINGLE_QUOTE).append(merchantData.getMerchantType()).append(SINGLE_QUOTE).append(",");
        builder.append(SINGLE_QUOTE).append(merchantData.getOfficialName()).append(SINGLE_QUOTE).append(",");
        builder.append("NOW()").append(",");
        builder.append("NOW()").append(",");
        builder.append("0").append(",");
        builder.append(merchantData.getIndustryTypeId());
        builder.append(");\n\n");
        return builder.toString();
    }
}
